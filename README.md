# Visualizacion de enfermedades

## 1.Introducción

Este proyecto surgió para dar uso a las funcionalidades de spark y tambien con
la intención de buscar algunas funcionalidades nuevas y que tuvieramos que 
lidiar con ellas para dar asi con un proyecto completo.El proyecto lo que hace 
es dada una base de datos que cogemos de la OMS, nos representa en un mapa los 
paises que se ven afectados por una enfermedad pero solo cuando superen un 
umbral que nosotros podemos elegir.

## 2.Entorno de trabajo

Para poder correr el proyecto, necesitaremos diferentes librerias ademas de 
tener instalado eclipse en nuestro ordenador.Las diferentes librerias que 
necesitamos seran para poder hacer funcionar spark,representar mapas y ademas 
buscar paises por nombre y devolver coordenadas. 

Son las siguientes:

   Bases de datos de la OMS (http://apps.who.int/gho/data/?theme=home)

   Libreria de spark utilizada (http://spark.apache.org/downloads.html)
   
   Pagina web de donde descargarnos las librerias para representar mapas
  (http://unfoldingmaps.org/tutorials/getting-started-in-eclipse.html)
  
   Pagina web para hacer las peticiones por nombre y que nos devuelva las coordenadas
  (http://www.geonames.org/export/)
  
## 3.Funcionalidad

 En cuanto a la funcionalidad se indicara de forma mas concreta el funcionamiento
 del programa, de tal manera que una vez descargada la base de datos la pasaremos 
 a el proyecto enfermedades y ejecutando el archivo main dentro del paquete 
 Ejecutar,obtendremos en pantalla unas opciones de entre las cuales tenemos que elegir.
 
 Por debajo se ha realizado el trabajo de spark y se han ido incorporando listas 
 de datos a varios ficheros que dependeran de la opción marcada. 
 
 Se guardaran para la opcion tres ficheros :
 
 1.Un fichero con el numero de casos por pais.
 
 2.Un fichero con los paises donde la enfermedad se ha dado.
 
 3.Un fichero con la opcion de enfermedad elegida.
 
 Con esto deberemos ir al segundo proyecto denominado mapita y a partir de ahi, 
 tenemos que meternos enel paquete mapita y ejecuta con java applet el fichero 
 SimpleMapApp.java el cual representara gracias al fichero Opcion, un mapa con 
 los paises donde el numero de casos de la enfermedad supere el umbral que 
 debemos introducir por teclado.
 
 Por debajo, lo que hace la apliacion es leer de los ficheros todos los datos, 
 y en un bucle va pidiendo coordenadas por nombre a una pagina web, se crea un 
 marker por cada pais solo si sobrepasa el umbral. De esta manera nos aparecera 
 una ventana con tantos markers como paises superen el numero de casos de la enfermedad elegida.
 
 La imagen de ejemplo que se puede ver en el proyecto, es un ejemplo de ejecución
 con la difteria (Opcion 1) y con un umbral de 4500.
 (https://gitlab.com/Pascual/visualizacion-de-enfermedades/blob/master/EjemploMapa.png)
 
## 4.Mejoras

 Hay varias mejoras que podemos realizar a nuestra aplicacion, contamos tres:
 
 1.La principal mejora que queremos implementar es quitar la opcion del umbral, 
 y además que por cada pais nos incorpore el numero de casos que se han producido.
 Dado a que usamos unfolding maps no podemos representar para cada marker el numero de casos.
 
 2.Otra de las mejoras que queremos llevar a cabo es juntar la aplicacion y que
 una vez hecha la mejora 1. Juntar ambos proyectos en uno solo sin mensajes de 
 aviso de spark ni pedir argumentos por consola.
 
 3.Como ultima mejora llevar la aplicacion a android.
 
 
